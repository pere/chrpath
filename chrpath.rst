=======
chrpath
=======

---------------------------------------
change the rpath or runpath in binaries
---------------------------------------

:Date:   2002-05-04
:Copyright: GNU General Public License 2 or later.
:Manual section: 1
:Manual group: change rpath/runpath in binaries

..
  license::

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

SYNOPSIS
========

**chrpath** [ **-v** \| **--version** ] [ **-d** \| **--delete** ] [
**-r** *<path>* \| **--replace** *<path>* ] [ **-c** \| **--convert** ]
[ **-l** \| **--list** ] [ **-h** \| **--help** ] *<program>* [
*<program>* ... ]

DESCRIPTION
===========

**chrpath** changes, lists or removes the rpath or runpath setting in
a binary. The rpath, or runpath if it is present, is where the runtime
linker should look for the libraries needed for a program.

OPTIONS
=======

**-v** \| **--version**
   Display program version number.

**-d** \| **--delete**
   Delete current rpath or runpath setting.

**-c** \| **--convert**
   Convert the rpath setting into a runpath setting.

**-r <path>** \| **--replace <path>**
   Replace current rpath or runpath setting with the path given. The new
   path must be shorter or the same length as the current path..

**-k** \| **--keepgoing**
   Do not fail on first error, but process all arguments before
   returning the error.

**-l** \| **--list**
   List the current rpath or runpath (default)

**-h** \| **--help**
   Show usage information.

EXIT STATUS
===========

**0**
   If all operations were successful

**>\ 0**
   if one of the operations failed. A failing operation terminates the
   program unless -k is specified.

BUGS
====

This program cannot create an RPATH tag if the ELF does not have one,
and it can only replace an RPATH with one of equal or shorter length.
(Moving ELF sections following a lengthened string table would be
difficult and error-prone at best, and is sometimes outright
impossible due to issues like limited ranges in jump instructions.)

AUTHOR
======

The chrpath program was written by Petter Reinholdtsen
<pere@hungry.com>, based on works by Geoffrey Keating
<geoffk@ozemail.com.au> and Peeter Joot <peeterjoot@protonmail.com>.

This manual page was originally written by Tollef Fog Heen
<tfheen@debian.org>, for the Debian GNU/Linux system (but may be used
by others).
